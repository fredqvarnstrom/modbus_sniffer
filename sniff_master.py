"""
Modbus command parser class.

Supported commands of WattNode Modbus meter:

    03 (0x03) Read Multiple Holding Registers
    04 (0x04) Read Multiple Input Registers
    16 (0x10) Write Multiple Holding Registers
    06 (0x06) Write Single Holding Register

    For more details about the commands, please refer:  http://www.simplymodbus.ca/FC03.htm

"""
import Queue
import threading

import datetime
import traceback

import serial
import time
from base import Base
import re
from PyCRC.CRC16 import CRC16
import binascii

command_list = ['03', '04', '16', '06']


class SniffMaster(Base):
    ser = None
    data_queue = Queue.Queue()
    b_stop = False
    read_cnt = 20
    slave_addr = '02'

    def __init__(self, slave_addr='02', port='/dev/ttyUSB1', baudrate=9600, read_cnt=50):
        Base.__init__(self)
        self.read_cnt = read_cnt
        self.slave_id = slave_addr
        self.ser = serial.Serial(port=port, baudrate=baudrate)

    def start(self):
        self.b_stop = False
        threading.Thread(target=self.run).start()

    def run(self):
        """
        Receive data from the master and parse it to modbus command
        :return:
        """
        buf = []  # Remaining hex values for each loop
        while True:
            if self.b_stop:
                time.sleep(.1)
                buf = []
            else:
                # Read hex values, convert it to hex strings, split them with 2-length values
                val = buf + re.findall('..', self.ser.read(self.read_cnt).encode('hex'))
                pos = 0
                while True:
                    try:
                        if val[pos] == self.slave_addr:
                            if val[pos + 1] in ['03', '04']:  # Read Multiple Holding/Input Registers
                                addr = int(val[pos + 2] + val[pos + 3], 16)
                                if not validate_register_address(addr):
                                    pos += 1
                                    continue
                                crc = CRC16(modbus_flag=True).calculate(binascii.a2b_hex(''.join(val[pos:pos + 8])))
                                if crc != 0:
                                    print "CRC Error, command: ", ''.join(val[pos:pos + 8]), ' | CRC: ', crc
                                    pos += 1
                                    continue
                                count = int(val[pos + 4] + val[pos + 5], 16)
                                tmp = dict()
                                tmp['cmd'] = val[pos + 1]
                                tmp['start_address'] = addr
                                tmp['count'] = count
                                tmp['timestamp'] = datetime.datetime.now()
                                self.data_queue.put(tmp)
                                pos += 8

                            elif val[pos + 1] == '06':  # Write Single Holding Register
                                addr = int(val[pos + 2] + val[pos + 3], 16)
                                if not validate_register_address(addr):
                                    pos += 1
                                    continue
                                crc = CRC16(modbus_flag=True).calculate(binascii.a2b_hex(''.join(val[pos:pos + 8])))
                                if crc != 0:
                                    print "CRC Error, command: ", ''.join(val[pos:pos + 8]), ' | CRC: ', crc
                                    pos += 1
                                    continue
                                value = val[pos + 4] + val[pos + 5]
                                tmp = dict()
                                tmp['cmd'] = val[pos + 1]
                                tmp['start_address'] = addr
                                tmp['value'] = value
                                tmp['timestamp'] = datetime.datetime.now()
                                self.data_queue.put(tmp)
                                pos += 8
                            elif val[pos + 1] == '16':  # Write Multiple Holding Registers
                                addr = int(val[pos + 2] + val[pos + 3], 16)
                                if not validate_register_address(addr):
                                    pos += 1
                                    continue
                                reg_num = int(val[pos + 4] + val[pos + 5], 16)
                                byte_num = int(val[pos + 6])
                                packet_len = byte_num + 9
                                crc = CRC16(modbus_flag=True).calculate(
                                    binascii.a2b_hex(''.join(val[pos:pos + packet_len])))
                                if crc != 0:
                                    print "CRC Error, command: ", ''.join(val[pos:pos + packet_len]), ' | CRC: ', crc
                                    pos += 1
                                    continue
                                value = ''.join(val[pos + 7:pos + 7 + byte_num])
                                tmp = dict()
                                tmp['cmd'] = val[pos + 1]
                                tmp['start_address'] = addr
                                tmp['count'] = reg_num
                                tmp['value'] = value
                                tmp['timestamp'] = datetime.datetime.now()
                                self.data_queue.put(tmp)
                                pos += packet_len
                            else:
                                pos += 1
                        else:
                            pos += 1

                    except IndexError:
                        # If current position reaches to the end, save remaining data and start reading again.
                        buf = val[pos:]
                        break
                    except:
                        traceback.print_exc()
                        time.sleep(10)


def validate_register_address(addr):
    """
    Check whether given address is valid for WattNode slave device...
    :return:
    """
    if 1000 <= addr < 1035 or 1100 <= addr < 1183 or 1200 <= addr < 1222 or 1300 <= addr < 1364 or 1600 <= addr < 1623 \
            or 1650 <= addr < 1656 or 1700 <= addr < 1724:
        return True
    else:
        return False


if __name__ == '__main__':

    s = SniffMaster('/dev/ttyUSB1')
    s.start()
    while True:
        if s.data_queue.qsize() > 0:
            print s.data_queue.get()
        time.sleep(.5)
