import os
from peewee import *


db = SqliteDatabase(os.path.dirname(os.path.realpath(__file__)) + '/sniff_data.db')
db.connect()


class SniffData(Model):
    """
    ORM class for Gateway Table.
    Because we have not specified a primary key,
    peewee will automatically add an auto-incrementing integer primary key field named id.
    """
    cmd = TextField()
    start_address = IntegerField()
    count = IntegerField(null=True)
    value = TextField(null=True)

    response = TextField()

    request_time = DateTimeField()
    response_time = DateTimeField()

    class Meta:
        database = db

if __name__ == '__main__':

    a = SniffData()
    all_items = a.select()
    print len(all_items)
    # for item in all_items:
    #     print item.response_time



