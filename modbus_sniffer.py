#!/usr/bin/python
import threading
import time

import datetime

from sniff_master import SniffMaster
import modbus_tk.defines as cst
import modbus_tk.modbus_rtu as modbus_rtu
import serial
import traceback
from base import Base
from orm import *


class ModbusSniffer(Base):

    master = None
    slave = None
    b_stop = False

    def __init__(self):
        Base.__init__(self)

    def initialize(self):
        try:
            self.master = SniffMaster(slave_addr=self.get_param_from_xml('SLAVE_ADDRESS'),
                                      port=self.get_param_from_xml('MASTER_PORT'),)

            self.slave = modbus_rtu.RtuMaster(serial.Serial(
                port=self.get_param_from_xml('SLAVE_PORT'),
                baudrate=int(self.get_param_from_xml('BAUDRATE')), bytesize=8, parity='N', stopbits=1, xonxoff=0))
            self.slave.set_timeout(1.0)
            self.slave.set_verbose(True)
            if not SniffData.table_exists():
                SniffData.create_table()
            return True
        except:
            traceback.print_exc()
            return False

    def read_slave(self, cmd_type=cst.READ_HOLDING_REGISTERS, starting_addr=1001, addr_count=1, data_type='integer'):
        """
        Read register values
        :param cmd_type: command type
        :param starting_addr:
        :param addr_count:
        :param data_type: 'float' or 'integer'
        :return:
        """
        slave_addr = int(self.get_param_from_xml('SLAVE_ADDRESS'))
        try:
            resp = self.slave.execute(slave=slave_addr, function_code=cmd_type, starting_address=starting_addr,
                                      quantity_of_x=addr_count)
            return resp
        except:
            traceback.print_exc()
            return None

    def write_slave(self, cmd_type=cst.WRITE_SINGLE_REGISTER, starting_address=100, values=''):
        """
        Write values to slave
        :param cmd_type:
        :param starting_address:
        :param values:
        :return:
        """
        slave_addr = int(self.get_param_from_xml('SLAVE_ADDRESS'))
        try:
            resp = self.slave.execute(slave=slave_addr, function_code=cmd_type, starting_address=starting_address,
                                      output_value=values)
            return resp
        except:
            traceback.print_exc()
            return None

    def start(self):
        self.b_stop = False
        threading.Thread(target=self.run).start()
        threading.Thread(target=self.export_data).start()

    def run(self):
        self.master.start()
        while True:
            if self.b_stop:
                self.master.b_stop = True
                time.sleep(.1)
            else:
                if self.master.data_queue.qsize() > 0:
                    req = self.master.data_queue.get()
                    if req['cmd'] in ['03', '04']:
                        resp = self.read_slave(cmd_type=int(req['cmd']), starting_addr=req['start_address'],
                                               addr_count=req['count'])
                        if resp is not None:
                            str_resp = ','.join(str(x) for x in resp)
                            SniffData.create(cmd=req['cmd'], start_address=req['start_address'], count=req['count'],
                                             request_time=req['timestamp'], response_time=datetime.datetime.now(),
                                             response=str_resp)
                    else:
                        resp = self.write_slave(cmd_type=int(req['cmd']), starting_address=req['start_address'],
                                                values=req['values'])
                        if req['cmd'] == '06':
                            SniffData.create(cmd=req['cmd'], start_address=req['start_address'],
                                             value=req['value'], request_time=req['timestamp'],
                                             response_time=datetime.datetime.now(), response=str(resp))
                        else:
                            SniffData.create(cmd=req['cmd'], start_address=req['start_address'], count=req['count'],
                                             value=req['value'], request_time=req['timestamp'],
                                             response_time=datetime.datetime.now(), response=str(resp))

                    print " ---------------"
                    print 'Request: ', req
                    print 'Response: ', resp

    def export_data(self):
        """
        Pull all data from local db and upload it to the remote postgreSQL db
        :return:
        """
        while True:
            try:
                export_time = datetime.datetime.strptime(self.get_param_from_xml('EXPORT_TIME'), "%H:%M:%S").time()
            except ValueError:
                print 'Invalid time value, please check config file and make sure correct value.  e.g. 13:12:15'
                time.sleep(60)
                continue

            if datetime.datetime.now().time() == export_time:
                print 'Reading whole data and exporting to the remote DB...'
                # TODO: implement logic...

            time.sleep(.1)

if __name__ == '__main__':

    print "Starting modbus sniffer..."

    a = ModbusSniffer()
    while not a.initialize():
        print 'Failed to connect to devices... please check connectivity'
        time.sleep(60)
    a.start()
    # s_time = time.time()
    # print a.read_slave(cst.READ_HOLDING_REGISTERS, 1700, 23)
    # print 'Elapsed: ', time.time() - s_time
